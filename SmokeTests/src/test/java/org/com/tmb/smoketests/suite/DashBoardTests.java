package org.com.tmb.smoketests.suite;

import java.util.concurrent.TimeUnit;

import org.com.tmb.smoketests.config.CommonConstants;
import org.com.tmb.smoketests.hooks.Hooks;
import org.com.tmb.smoketests.locators.DashBoardLocators;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.relevantcodes.extentreports.LogStatus;

public class DashBoardTests extends Hooks {
	
	SoftAssert sa= new SoftAssert();

	
		@Parameters({ "browser" })
		@BeforeMethod
		public void openBrowser(String browserName){
			System.out.println("Opening the test in " +browserName+ "Browser");
			Hooks.OpenBrowser(browserName);
			Hooks.getOSName();
		}

	/* this test basically picks up
	 * navigate to the dashboard Registrations option
	 * picks up the Registrations value till date
	 * select the Report type as detailed brand performance and pickup the registrations value
	 * compare both values are equal
	 * */
	@Test(priority=1)
	public void testRegistrationsReportType() throws InterruptedException
	{
		driver.manage().deleteAllCookies();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(DashBoardLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(DashBoardLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(DashBoardLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		System.out.println("User, logged in successfully.");
		System.out.println("in the verifyRegistrationsReportType ");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		String HeaderValue= driver.findElement(By.xpath("//div[@class='result-information']/span[1]")).getText();
		System.out.println("1=============>");
		System.out.println(HeaderValue);
		Select reportType = new Select(driver.findElement(By.id("field-reportType_id")));
		System.out.println("reportType======>"+reportType);
		reportType.selectByIndex(1);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		String reportValue= driver.findElement(By.xpath("//div[@class='result-information']/span[1]")).getText();
       System.out.println(reportValue);       
       sa.assertEquals(HeaderValue, reportValue);
       if(HeaderValue.equals(reportValue))
		{
			System.out.println("Test Passed, Expected HeaderValue and reportValue are Equal");
			et.log(LogStatus.PASS,"Test Passed, Expected Deliveries_count and map_value are Equal");

		} else
		{
			System.out.println("Test Failed, Expected HeaderValue and reportValue are not Equal");
			et.log(LogStatus.FAIL,"Test Failed, Expected Deliveries_count and map_value are not Equal");
		}
       sa.assertAll();
	}
	
	/* this test basically picks up
	 * navigate to the dashboard and selects deliveries option
	 * picks up the deliveries value till date
	 * Verify the text is definitely Deliveries
	 * And fails if the delivers value has an error text
	 * */
	@Test(priority=2)
	public void testDeliveryReportType() throws InterruptedException
	{
		driver.manage().deleteAllCookies();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		driver.findElement(DashBoardLocators.TMB_UserName).sendKeys(CommonConstants.UserName);
		driver.findElement(DashBoardLocators.TMB_Password).sendKeys(CommonConstants.password);
		driver.findElement(DashBoardLocators.TMB_Submit_Button).click();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		System.out.println("User, logged in successfully.");
		System.out.println("in the verifyDeliveryReportType ");
		String errorText ="Error loading Deliveries totals!";
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		Select reportType = new Select(driver.findElement(By.id("field-dashboard_id")));
		reportType.selectByVisibleText("Deliveries");
		String reportHeader=driver.findElement(By.xpath("//*[@id=\"reportHeader\"]/div/div[1]/h1")).getText();
		System.out.println("reportHeader value "+reportHeader);
		reportType.selectByVisibleText("Deliveries");
		Thread.sleep(3000);
		String reportValue= driver.findElement(By.xpath("//div[@class='result-information']/span[1]")).getText();
        System.out.println(reportValue);       
       sa.assertEquals("Deliveries", reportHeader);
       boolean result=false;
       if (errorText.equalsIgnoreCase(reportValue))
    	   result=true;
       sa.assertFalse(result);
       sa.assertAll();
	}
	
	@AfterMethod
	public void closeDriver(){
		
		System.out.println("Closing the Driver");
		driver.manage().deleteAllCookies();
		driver.quit();
		System.out.println("Closed the Driver");
		er.flush();
		er.endTest(et);
	}	


}
