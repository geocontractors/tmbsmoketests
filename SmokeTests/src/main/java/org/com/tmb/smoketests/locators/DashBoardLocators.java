package org.com.tmb.smoketests.locators;

import org.openqa.selenium.By;


public class DashBoardLocators
{
	
	//landing page elements
	public static By TMB_UserName = By.cssSelector("#UserEmail");
	public static By TMB_Password = By.cssSelector("#UserPassword");
	public static By TMB_Submit_Button =By.cssSelector("#loginformbutton");
	
	 //select the Dashboard type as Deliveries
	
	public static By DashBoard_Type = By.cssSelector("#field-dashboard_id");
	
}

