package org.com.tmb.smoketests.hooks;

import org.com.tmb.smoketests.config.CommonConstants;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;



public class Hooks 
{
	
	//spublic static WebDriver driver;	
	public static WebDriver driver;	

	//To get Current System date
  	static DateFormat dateformat=new SimpleDateFormat("_yyyy-MM-dd_hh-mm-ss a");
  	static Date date1=new Date();
  	static String CurrentExecution = dateformat.format(date1);
	
	
	//To get Result in html view
  	
  	public static ExtentReports er=new ExtentReports("ExtentReports\\Log"+CurrentExecution+".html");
  	public static ExtentTest et=er.startTest("TMB Smoke Test Results");
	
	public static void OpenBrowser(String bname)	
	{

		
	if(System.getProperty("os.name").contains("Windows"))
	{
		
		if(bname.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver",CommonConstants.ChromeDriverPath);
			driver=new ChromeDriver();
			driver.manage().deleteAllCookies();

		}
		else if(bname.equalsIgnoreCase("iexplore"))
		{
			System.setProperty("webdriver.ie.driver",CommonConstants.IEDriverPath);
			//System.setProperty("webdriver.ie.driver", CommonConstants.IEDriverPath);
			System.setProperty(InternetExplorerDriverService.IE_DRIVER_LOGLEVEL_PROPERTY,"INFO");      
			System.setProperty(InternetExplorerDriverService.IE_DRIVER_LOGFILE_PROPERTY, "C:\\IE.log");
			DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			driver=new InternetExplorerDriver(); 
		}
		else if(bname.equalsIgnoreCase("mozilla"))
		{
			System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\firefox.log");
			FirefoxOptions options = new FirefoxOptions();
			//setting the page load startegy to none
			options.setPageLoadStrategy(PageLoadStrategy.NONE);
			ProfilesIni profini =new ProfilesIni();
			FirefoxProfile prof=profini.getProfile("automation");
			//if any notifications, it disables for this firefox profile.
			prof.setPreference("dom.webnotifications.enabled", "false");
			prof.setAcceptUntrustedCertificates(true);
			prof.setAssumeUntrustedCertificateIssuer(false);
			options.setProfile(prof);
			//automation
			System.setProperty("webdriver.gecko.driver",CommonConstants.FirefoxDriverPath);
			driver=new FirefoxDriver(options);
			

		}
		
	

	}
	else if(System.getProperty("os.name").contains("Mac"))
	{
		if(bname.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver",CommonConstants.Mac_ChromeDriverPath);
			driver=new ChromeDriver();
		}
		else if(bname.equalsIgnoreCase("IE"))
		{
			System.setProperty("webdriver.ie.driver",CommonConstants.Mac_IEDriverPath);
			driver=new InternetExplorerDriver(); 
		}
		else if(bname.equalsIgnoreCase("mozilla"))
		{
			System.setProperty("webdriver.gecko.driver",CommonConstants.Mac_FirefoxDriverPath);
			driver=new FirefoxDriver();
		}
		
	
		
	}
	
		driver.manage().deleteAllCookies();
		driver.get(CommonConstants.TMB_STG_URL);
		driver.manage().window().maximize();
		WebDriverWait wd=new WebDriverWait(driver, 25);
		//wd.until(ExpectedConditions.visibilityOfElementLocated(PriceTarrifLocator.Energy_link));
	
}
	
	
	public static String getData()	
	{

	String data="";	
	if(System.getProperty("os.name").contains("Windows"))
	{	
		data = System.getProperty("user.dir")+"\\TestData\\data.xlsx";
	}

	
	
	else if(System.getProperty("os.name").contains("Mac"))
	{	
		data = System.getProperty("user.dir")+"/TestData/data.xlsx";
	}

	return data;
}
	
	public static String getOSName()	
	{

	String osName="";	
	if(System.getProperty("os.name").contains("Windows"))
	{	
		osName = "Windows";
		System.out.println("Operating System Name is  "+osName);

	}

	
	else if(System.getProperty("os.name").contains("Mac"))
	{	
		osName = "Mac";
		System.out.println("Operating System Name is  "+osName);

		
	}

	return osName;
}	
}
